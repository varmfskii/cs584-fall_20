#!/bin/env python
import networkx as nx
import matplotlib.pyplot as pp

G=nx.DiGraph()
f=open("twitter_combined.txt","r")
all=f.read()
f.close()
lines=all.split('\n')
count=0
for i in range(0,50000):
    edge=lines[i].split(' ')
    if len(edge)>1:
        G.add_edge(edge[0],edge[1])
print(nx.info(G))
pos=nx.spring_layout(G)
col=[20000*G.degree(v) for v in G]
print("degree centrality")
cent=nx.degree_centrality(G)
sz=[v*2500 for v in cent.values()]
pp.figure(figsize=(20,20))
pp.title('degree centrality')
nx.draw_networkx(G, pos=pos, with_labels=False, node_size=sz, node_color=col)
pp.savefig('degree.eps')
pp.close()
print("betweenness centrality")
cent=nx.betweenness_centrality(G, normalized=True)
sz=[v*2500 for v in cent.values()]
pp.figure(figsize=(20,20))
pp.title('betweenness centrality')
nx.draw_networkx(G, pos=pos, with_labels=False, node_size=sz, node_color=col)
pp.savefig('betweenness.eps')
pp.close()
print("eigenvector centrality")
cent=nx.eigenvector_centrality_numpy(G)
sz=[v*5000 for v in cent.values()]
pp.figure(figsize=(20,20))
pp.title('eigenvector centrality')
nx.draw_networkx(G, pos=pos, with_labels=False, node_size=sz, node_color=col)
pp.savefig('eigenvector.eps')
pp.close()
print("load centrality")
cent=nx.load_centrality(G)
sz=[v*5000 for v in cent.values()]
pp.figure(figsize=(20,20))
pp.title('load centrality')
nx.draw_networkx(G, pos=pos, with_labels=False, node_size=sz, node_color=col)
pp.savefig('load.eps')
pp.close()
print("closeness centrality")
cent=nx.closeness_centrality(G)
sz=[v*1250 for v in cent.values()]
pp.figure(figsize=(20,20))
pp.title('closeness centrality')
nx.draw_networkx(G, pos=pos, with_labels=False, node_size=sz, node_color=col)
pp.savefig('closeness.eps')
pp.close()
