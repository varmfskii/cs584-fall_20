#!/bin/env python
import networkx as nx
import matplotlib.pyplot as pp

G=nx.read_edgelist('twitter_combined.txt', create_using=nx.DiGraph(), nodetype=int)
print(nx.info(G))
cent=nx.load_centrality(G)
print('calculated centrality')
v=list(cent.values())
v.sort(reverse=True)
nx.set_node_attributes(G,cent,'cent')
for key in cent.keys():
    if cent[key]<v[999]:
        G.remove_node(key)

print('pruned graph')
print('load centrality')
print(nx.info(G))
d=G.nodes(data='cent')
sz=[10000*v[1] for v in d]
pos=nx.spring_layout(G)
pp.figure(figsize=(20,20))
nx.draw_networkx(G, pos=pos,with_labels=False, node_size=sz)
pp.savefig('load_core.eps')
pp.close()
